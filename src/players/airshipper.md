# Airshipper

![Airshipper](https://songtronix.com/airshipper-0.4.0.gif)

Airshipper is a cross-platform Veloren launcher taking care of keeping Veloren up to date.
Currently due to our irregular and frequent updates we recommend using it.

### Download

Visit the [download page](https://veloren.net/download/) to download Airshipper.

### Files

Logs, screenshots, assets will all be located in the respective profile depending on your operating system.

|   OS    |                    Path                    |
| :-----: | :----------------------------------------: |
| Windows |           `%appdata%/airshipper`           |
|  Linux  |        `~/.local/share/airshipper`         |
|  MacOS  | `~/Library/Application Support/airshipper` |

### Troubleshooting

Incase airshipper does not open or display correct you can use the cli by

1. Opening a terminal

   > On Windows press `Windows key + R`. Then type `cmd` and hit `enter`.

2. Type `airshipper run` and hit enter
3. Enjoy the game.
